#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <time.h>
#include "banker.h"


void *run(void *p){	
	
	int resources[M];
	
	int i = 0;
	int j = 0;
	int r;
	int pid;
	int range;
	pid = *(int *)p;
	int good = 0;
	int zeros = 0;
	
	while(1){			
		
		pthread_mutex_lock(&mut);
	
		i = N;
		for(j = 0; j < N; j++){			//Checks to see if there is only 1 thread still alive
			
			if(finished[0][j] == 1)
				i--;				
		}
		if(i == 1)
			break;
			
			
		printf("\nProcess %d, requesting resources : ", pid);			
	
		while(1){									
			for(i = 0; i < M; i++){		
				range = need[pid][i];
			
				if(range == 0)
					resources[i] = 0;
				else if(max[pid][i] == 1 || range == 1){
					resources[i] = 1;				
				}		
				else{
					resources[i] = rand() % (need[pid][i] + 1);
				}
			
				if(resources[i] == 0)
					zeros++;			
			}
		
			if(zeros == M){					//Don't want to request all zeros
			zeros = 0;			
			}
			else{
				zeros = 0;
				break;	
			}			
		}	
	
		for(i =0; i < M; i++)
			printf(" %d ", resources[i]);
		
		good = request_resources(pid, resources);
	
		if(good == 0){
			printf("\nSafety State = SAFE");
			for(i = 0; i < M; i++){
				available[0][i] -= resources[i];
				need[pid][i] -= resources[i];
				allocated[pid][i] += resources[i];
		
			}
		printf("\nAvailable Resources :");	
	
		for(i = 0; i < M; i++)	
			printf(" %d ", available[0][i]);
		
			printf("\n");
			
		}
		
		else{
			printf("\nSafety State = UNSAFE");
		
			for(i = 0; i < M; i++)
				resources[i] = allocated[pid][i];
			
			release_resources(pid, resources);
			printf("\nProcess %d Terminating \n", pid);
			finished[0][pid] = 1;
		
			for(i = 0; i < M; i++){					//0's in the arrays
				max[pid][i] = 0;
				need[pid][i] = 0;			
			}
		
			pthread_mutex_unlock(&mut);
			sleep(rand() % 4);		
		
			break;		
		}	
		
		pthread_mutex_unlock(&mut);
		sleep(rand() % 4);
	
		pthread_mutex_lock(&mut);
	
		zeros = 0;
		
		while(1){
			for(j = 0; j < M; j++){
				if(allocated[pid][j] == 0)
					resources[j] = 0;
				else	
				resources[j] = rand() % (allocated[pid][j] + 1);
				
				if(resources[j] == 0)
					zeros++;		
			}			
			if(zeros == M)			//can't release all 0's
				zeros = 0;
			else{
				zeros = 0;
				break;
			}
		}		
			
		release_resources(pid, resources);
		
		pthread_mutex_unlock(&mut);
		sleep(rand() % 4);
	
	}
		
}
