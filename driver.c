#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <time.h>
#include "banker.h"


void build_need(){

	int i = 0;
	int j = 0;
	need = malloc(N * sizeof(int *));
	
	//initialize the need array
	for(i = 0; i < N; i++){
		need[i] = malloc(M * sizeof(int));			
	}	
	//add the random numbers to the allocated array
	for(i = 0; i < N; i++){	
		for(j = 0; j < M; j++){			
				need[i][j] = max[i][j] - allocated[i][j];	
		}	
	}
}

void build_allocated(){

	int i = 0;
	int j = 0;
	allocated = malloc(N * sizeof(int *));
	
	//initialize the allocated array
	for(i = 0; i < N; i++){
		allocated[i] = malloc(M * sizeof(int));			
	}	
	//add the random numbers to the allocated array
	for(i = 0; i < N; i++){	
		for(j = 0; j < M; j++){							
			allocated[i][j] = 0;			
		}	
	}
}

void build_max(){

	int i = 0;
	int j = 0;
	int zeros = 0;
	int do_again = 0;
	max = malloc(N * sizeof(int *));
	
	//initialize the max array
	for(i = 0; i < N; i++){
		max[i] = malloc(M * sizeof(int));
	}
	
	//add the random numbers to the max array	
	while(1){
		for(i = 0; i < N; i++){	
			for(j = 0; j < M; j++){
				int temp = available[0][j] + 1;		
					
				if(temp == 1)
					max[i][j] = 1;
				else	
					max[i][j] = rand() % temp;
			
				if(max[i][j] == 0)
					zeros++;
			}	
			
			if(zeros == M){						//Don't want a Max vector of all zeros!!!			
				zeros = 0;
				do_again = 1;
				break;
			}
		}
			
		if(do_again == 0){
			break;
		}
		else
			do_again = 0;		
	}	
}

void launch_threads(){

	srand(time(NULL));
	pthread_t thread[N];
	
	pthread_t t1;
	pthread_t t2;
	int id, id1, id2;
	int	arg1 =1;
	int	arg2 =2;	
		
	int i = 0;
	
	id = pthread_mutex_init(&mut, NULL);
	
	for(i =0; i < N; i++){
		pthread_create(&thread[i], NULL, run, (void *)&i);
		
	}	
	for(i = 0; i < N; i++)
		pthread_join(thread[i], NULL);	

}

int main(int argc, char ** argv){
	
	N = atoi(argv[2]);			//number of processes(threads)
	M = argc - 4;				//number of resources
	
	srand(time(NULL));
	
	finished = malloc(sizeof(int *));
	finished[0] = malloc(N * sizeof(int));
	available = malloc(N * sizeof(int *));		//allocate space for available vector
	available[0] = malloc(M * sizeof(int));
	
	int i = 4;	
	int j = 0;
	
	if((strcmp(argv[1], "-n") != 0) || strcmp(argv[3], "-a") != 0){
		printf("\nError: Proper usage ./bankers -n <numprocs> -a <available vector>\n");
		return -1;
	}
	
	//builds the finished vector
	for(i = 0; i < N; i ++)
		finished[0][i] = 0;
	
	//builds the available vector
	for(i =4; i < argc; i++){
		available[0][j] = atoi(argv[i]);
		
		if(available[0][j] <= 0){
			printf("\nError: Available resources must be a positive number!\n");
			return -1;
		}		
		j++;		
	}
	
	printf("\nAvailable");
	
	for(i = 0; i < M; i++)
		printf(" %d", available[0][i]);
	 
	 printf("\n");
	 
	build_max();
	
	printf("\nMax\n");
	
	for(i = 0; i < N; i++){		
		for(j = 0; j < M; j++){			
			printf("%d ", max[i][j]);		
		}
		printf("\n");	
	}
	
	build_allocated();
	
	printf("\nAllocated\n");
	
	for(i = 0; i < N; i++){		
		for(j = 0; j < M; j++){			
			printf("%d ", allocated[i][j]);		
		}
		printf("\n");	
	}
	
	build_need();
	
	printf("\nNeed\n");
	
	for(i = 0; i < N; i++){		
		for(j = 0; j < M; j++){			
			printf("%d ", need[i][j]);		
		}
		printf("\n");	
	}
	
	launch_threads();
	
	printf("\nSimulation has Completed\n");
	
		free(allocated);
		free(max);
		free(need);
		free(available);
	 
	return 0;

}