
int N, M;	//N - number of processes, M number of resources
int **available;	
int **max;
int **allocated;
int **need;
int request_resources();
void release_resources();
void launch_threads();
int **finished;
void *run();
pthread_mutex_t mut;
