#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <time.h>
#include "banker.h"





int is_safe(int temp_allocated[N][M], int temp_available[N][M], int temp_need[N][M]){

	int completed[N];
	int safe = 0;
	int k = 0;
	int count1 = 0;
	int count2 = 0;
	int i = 0;
	int j = 0;
	
	for(i = 0; i < N; i++)
		completed[i] = 0;

	while(count1!=N){
		count2=count1;
		for(i=0;i<N;i++){			
			for(j=0;j<M;j++){		     
				if(temp_need[i][j]<=temp_available[0][j]){	            
					k++;				//increment k, this is good
				}       
			}   		
		//meaning if every resource can be allocated given the available resources
			if(k==M && completed[i]==0 ){		
				completed[i]=1;					
				count1++;           	
			}
			k=0;					
			//try another process
		}
        if(count1==count2){			//if count1 doesnt increase, deadlock         
			safe = -1;
			break;
		} 
	}
	
	return safe;
}



int request_resources(int pid, int resources[]){

	int i = 0;
	int j = 0;
	int safe = 2;
	int count = 1;
	int temp_allocated[N][M];
	int temp_available[1][M];
	int temp_need[N][M];
	int ready = 0;


	count = 0;
	
	for(i = 0; i < M; i++){	
		if(resources[i] > available[0][i]){		//if the requested resource is greater than what is currently available
				
			safe = -1;
			break;
		}
			count++;
	}
	
	//the resources are currently available
	if(count == M){				//check if the state is safe after resources are allocated	
		//copy array elements to be sent
		for(i =0; i < N; i++){		
			for(j = 0; j < M; j++){			
				//in the case that i is the requesting process, need to update these arrays before we send them to be checked
				if(i == pid){
					temp_allocated[i][j] = allocated[i][j] + resources[j]; 
					temp_need[i][j] = need[i][j] - resources[j];				
				}
				else{
					temp_allocated[i][j] = allocated[i][j];
					temp_need[i][j] = need[i][j];
				}		
				
				if(i == 0) //just need to do this once, i could really be any number
					temp_available[0][j] = available[0][j] - resources[j];		//what is available after the resources were allocated
			}
			
		}
		safe = is_safe(temp_allocated, temp_available, temp_need);
	}
	else{		
		//TERMINATE	
	}	
	
	return safe;
}

void release_resources(int pid, int resources[]){

	int i = 0;
	int j = 0;
	
	for(j = 0; j < M; j++){
	
		available[0][j] += resources[j];
		allocated[pid][j] -= resources[j];
		need[pid][j] += resources[j];
	}
	
	printf("\nProcess %d, releasing resources : ", pid);
	
	for(i = 0; i < M; i++)
			printf(" %d ", resources[i]);
		
	printf("\nAvailable Resources :");		
	for(i = 0; i < M; i++)
			printf(" %d ", available[0][i]);
			
		printf("\n");
				
	

}
